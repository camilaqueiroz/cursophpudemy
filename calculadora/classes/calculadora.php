<?php 

class Calculadora {
	private $total;
	private $numeroUm;
	private $numeroDois;

	function __construct()
	{
		$this -> total = 0;
		$this -> numeroUm = 0;
		$this -> numeroDois = 0; 
	}

	public function setNumeroUm($p_NumeroUm) {
		$this -> numeroUm = $p_NumeroUm;
	}
	public function setNumeroDois($p_NumeroDois) {
		$this -> numeroDois = $p_NumeroDois;
	}

	//operacoes
	public function somar(){
		$this -> total = $this -> numeroUm + $this -> numeroDois;
	}

	public function subtrair(){
		$this -> total = $this -> numeroUm - $this -> numeroDois;
	}

	public function multiplicar(){
		$this -> total = $this -> numeroUm * $this -> numeroDois;
	}

	public function dividir(){
		$this -> total = $this -> numeroUm / $this -> numeroDois;
	}

	public function getTotal() {
	 	return $this -> total;
	}

}


?>