<?php 
require_once 'classes/calculadora.php';

$numeroUm = $_POST['numeroUm'];
$numeroDois = $_POST['numeroDois'];
$operacao = $_POST['operacao'];

$calculadora = new Calculadora();

//Setar valores
$calculadora -> setNumeroUm($numeroUm);
$calculadora -> setNumeroDois($numeroDois);

switch ($operacao) {
	case 'somar':
		$calculadora -> somar();
		break;

		case 'subtrair':
		$calculadora -> subtrair();
		break;

		case 'multiplicar':
		$calculadora -> multiplicar();
		break;

		case 'dividir':
		$calculadora -> dividir();
		break;
	
	default:
		echo 'Operação não escolhida';
		break;
}

echo $calculadora -> getTotal();
	
?>