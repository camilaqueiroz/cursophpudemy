<?php 
/* 
Valor Total: R$ 800,00	
Valor do desconto: 10%
Valor total com desconto:
*/
require_once("funcoesDesconto.php");

$valorTotal = 800;
$desconto = 10;
$valorComDesconto = calculaDesconto($valorTotal, $desconto);


?>

Valor Total:R$ <?php echo $valorTotal ?>
<br>
Valor do desconto: <?php echo $desconto?>%
<br>
Valor total com desconto: R$ <?php echo $valorComDesconto ?>
