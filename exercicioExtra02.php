<!DOCTYPE html>
<html>
<head>
	<title>Exercício extra 02</title>
	<link href="calculadora/css/style.css" rel="stylesheet">
</head>
<body>
	<div class="row mt-3">
		<div class="column">
			<h1>Exercicio Extra 02</h1>
		</div>
	</div>
	<?php

	$numInicial =   $_POST['numInicial'];
	$numFinal = $_POST['numFinal'];

	if ($numInicial % 2 == 0) {
		echo 'O número inicial precisa ser ímpar';
	}
	else {
		for ($i = $numInicial; $i <= $numFinal; $i = $i + 2) { 
			echo "$i - ";
		}
		echo 'Finalizado!';
	}


	?>
	<form name="form2" method="post" action="exercicioExtra02.php">
		<div class="row mt-3">
			<div class="column">
				<label>Numero inicial (ímpar)</label>
				<input type="text" name="numInicial" maxlength="3" class="inputTop">
			</div>
			<div class="column">
				<label>Número final</label>
				<input type="text" name="numFinal" maxlength="3" class="inputTop">
			</div>
		</div>
		<div class="row mt-3">
			<div class="column">
				<input type="submit" class="btnCalcular" value="Enviar">
			</div>
			<div class="column">
				<input type="submit" value="Limpar">
			</div>
		</div>
	</form>
</body>
</html>