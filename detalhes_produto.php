<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Detalhes do produto</title>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
</head>
<body>

<div class="row">
	<div class="col-sm-12">
			<h1>Detalhes do produto</h1>
	</div>
</div>
<div class="row">
	<div class="col-sm-12">
		<?php
	    	$idProduto = $_POST['id_produto']; 
	    	/*GET = parâmetros visiveis na URL
	    	POST = Parâmetros do formulário são enviados por pacote, não aparecem na url. Nivel a mais de segurança em comparação ao GET */
	    	$detalhes[1] = "Detalhes das cadeiras";
	    	$detalhes[2] = "Detalhes do fogão";
	    	$detalhes[3] = "Detalhes do roteador";
	    	$detalhes[4] = "Detalhes da TV 29";
	    	echo $detalhes[$idProduto];
	    ?>
    </div>
</div>
</body>
</html>