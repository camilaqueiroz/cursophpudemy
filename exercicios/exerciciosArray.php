<?php 

$texto = "Nós não desistiremos nem fracassaremos. Nós iremos até o fim. Nós lutaremos na França, lutaremos nos mares e oceanos, nós lutaremos com confiança crescente e uma força também crescente ao nosso redor. Nós defenderemos nossa ilha, qualquer que seja o preço. Nós lutaremos nas praias, lutaremos em terra me, lutaremos nos campos e nas ruas, lutaremos nas montanhas. Nós nunca nos renderemos! Nós nunca nos renderemos";

$textoArray = explode(" ", $texto);
$textoArray = array_unique($textoArray);

foreach ($textoArray as $word) {
	if (substr($word, -1) == "s") {
		echo $word ."<br>";
	}
}
echo "<br>";
echo "<br>";
echo "<br>";

// Array associativo

$chaves = ["nome", "idade", "conjuge", "filhos"];
$valores = ["Marta", 23, "Fernando", "Huguinho, Zezinho, Luizinho"];
$i = 0;
foreach ($chaves as $dados) {
	$array = ["$dados" => "$valores[$i]"];
	$i++;
	var_dump($array);
}

?>