<?php 


//classe mãe/ superclasse
class Felino {

	var $mamifero = 'Sim';

	function correr() {
		echo 'Correr como felino';
	}
}

//Classe filha ou subclasse
class Chita extends Felino {

	//Polimorfismo
	function correr() {
		echo 'Correr como chita 100Km/h';
	}

}

$chita = new Chita();
echo $chita-> mamifero;
echo "<br />";
echo $chita-> correr();

?>