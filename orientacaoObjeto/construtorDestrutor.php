<?php 

class Pessoa {
	private $nome;

	public function correr() {
		echo $this -> nome . " correndo <br/>";
	}

	function __construct($parametroNome) {
		echo "Construtor iniciado <br/>";
		$this -> nome = $parametroNome;
		echo $this -> nome . "<br>";
	}

	function __destruct() {
		echo "objeto removido";
	}
}

$pessoa = new Pessoa('Camila');
$pessoa ->correr();

?>