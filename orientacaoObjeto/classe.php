<?php 

class Pessoa {
	//Atributos
	var $nome;

	//Métodos - getters and setters
	function setNome($nomeDefinido) {
		$this-> nome = $nomeDefinido;
	}

	function getNome() {
		return $this-> nome;
	}
}

$pessoa = new Pessoa();

//Acessa o método setNome passando o atributo 'Camila'
$pessoa-> setNome('Camila');
// Acessa o método getNome e retorna o valor do atributo nome
echo $pessoa-> getNome();

echo "<br />";

class Casa {

	//Atributos
	var $janela;
	var $telha;
	var $portão;
	var $piso;

	//Métodos

	function openJanela() {
		$this-> janela = true;
	}
	function telhaCor() {
		$this-> telha = 'Vermelha';
	}
	function portaoMaterial() {
		$this-> portao = 'Madeira';
	}
	function pisoTipo() {
		$this-> piso = 'Porcelanato';
	}
}

$casa = new Casa();

$casa-> telhaCor();
echo $casa-> telha;


?>