<?php 

class Veiculo {
	/*
	Tipos de visibilidade:
	* Se não declaramos visibilidade em membros (atributos e métodos) de uma classe automaticamente Será do tipo public.

	Public =  Atributos ou métodos como public podem ser acessados de forma livre, a partir da própria classe, a partir de classes descendentes e a partir de programas que fazem uso dessa classe.

	Private = Atributos ou métodos declarados como private só podem ser acessados dentro do escopo da própria classe em que foram declarados.

	Protected = Atributos ou métodos declarados com protected somente podem ser acessadas dentro da própria classe ou a partir de classes descendentes (herdadas).

	Encapsulamento:

	O encapsulamento é o ato de você provê uma proteção de acesso aos membros internos de um objeto. A classe é responsável por seus atributos, e dessa forma podemos acessar esses atributos apenas com métodos da própria classe, ou seja, criamos métodos dentro dessa classe para alterar os atributos. Os atributos (propriedades) nunca devem ser acessadas de fora da classe, pois assim temos uma segurança maior sobre seus valores.

	*/

	private $placa;
	private $cor;
	protected $tipo = 'Caminhonete';

	public function setPlaca($parametroPlaca){
	$this-> placa = $parametroPlaca;
	}

	public function getPlaca() {
		return $this-> placa;
	}

}

$veiculo = new Veiculo();
$veiculo-> setPlaca('CML1995');
echo $veiculo-> getPlaca();


class Carro extends Veiculo {
	public function exibirTipo() {
		echo $this-> tipo;
	}	
}

$carro = new Carro();
$carro-> exibirTipo();

?>