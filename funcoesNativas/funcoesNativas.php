<?php 

// isset(var) verifica se a variavel foi iniciada (se tem a variável);
$valor = 22;
if (isset($valor)) {
	echo 'Variavel iniciada';
}

//
//empty(), só retorna true quando => '',0,'0', false, null, array();
echo "<br>";
echo "<br>";
echo "<br>";

if (empty($valorEmpty)) {
	echo 'Variavel vazia';
}

// is_numeric() => retorna true quando for número
echo "<br>";
echo "<br>";
echo "<br>";

$valorNumeric = '22.3'; //Aceita string
if (is_numeric($valorNumeric)) {
	echo 'Valor numérico';
}

?>